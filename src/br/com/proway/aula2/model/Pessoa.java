package br.com.proway.aula2.model;

public class Pessoa {
    public String nome;
    public int idade;
    public boolean viva = true;

    public void andar(int passos){
        if(viva){
            System.out.println(nome + " está andando...");
            for(int i=0; i<passos; i++){
                System.out.print(i+1+"# ");
            }
            System.out.println("");
            System.out.println(nome + " está imovel.");
        }else{
            System.out.println("Infelizmente " + nome + " nao podera andar, pois encontra-se em outro plano.");
        }
    }

    public void morrer(String causa){
        viva = false;
        System.out.println(nome + " morreu de " + causa + " aos " + idade + " anos.");
    }

}
