package br.com.proway.aula2;

import br.com.proway.aula2.model.Pessoa;

public class Principal {

    static public boolean isViva(Pessoa pessoa){
        String status;

        // Testa se ta viva
        if(pessoa.viva){
            status = "viva";
        }else{
            status = "morta";
        }

        System.out.println(pessoa.nome + " está " + status + ".");

        return pessoa.viva;
    }


    public static void main(String[] args){

        Pessoa p1 = new Pessoa();
        p1.nome = "Dona senhora";
        p1.idade = 113;

        isViva(p1);

        p1.andar(20);

        p1.morrer("acidente de paramotor");

        p1.andar(342);

        isViva(p1);

    }
}
